# M3-TP
  
2.1 Gene name  
Gene name : “olfactory receptor 82a Spodoptera frugiperda”  
Accession number gene : 118277769  
Accession number protein : XP_035452568.1  
Genome assembly : AGI-APGP_CSIRO_Sfru_2.0 (GCF_023101765.2)  
Bioproject : PRJNA809428  
Publication : 
Proc Natl Acad Sci U S A2023
A tale of two copies: Evolutionary trajectories of moth pheromone receptors
Z Li, et al.


2.2 Gene ID  
Gene name : odorant receptor 13a-like  
Chromosomal localization : chr18, NC_036204.1 (2318819..2324740)
BLASTN : 

